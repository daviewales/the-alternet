# The Alternet

uBlacklist subscription to block all sites with trackers

## Description

[uBlacklist](https://iorate.github.io/ublacklist/docs) is a browser extension
which allows you to remove specific sites from search engine results (e.g.
Google, DuckDuckGo, etc.) uBlacklist allows you to subscribe to public blocking
rulesets. **The Alternet** is a uBlacklist ruleset which blocks _all_ websites
by default, except for specific whitelisted sites. The criteria for being added
to the whitelist is having no tracker scripts.

## Installation

See the uBlacklist
[subscription](https://iorate.github.io/ublacklist/docs/advanced-features#subscription)
docs.

Just add
`https://gitlab.com/daviewales/the-alternet/-/raw/main/the-alternet.txt` as
a subscription in uBlacklist!

## Roadmap

Currently, this subscription only whitelists my personal site and Wikipedia,
because those are the only sites I know off the top of my head which don't have
trackers. I'm planning to build a web crawler to mine the web for other
tracker-free sites. I'm thinking of using the uBlock Origin tracker lists for
this.

## Contributing

If you have a site with no trackers, please submit a pull request to add it to
the whitelist!

## License

This project is released under the MIT license

